'use strict';

// Dependencies
var through = require('through2'),
    pluginError = require('plugin-error'),
    replaceExtension = require('replace-ext'),
    hljs = require('highlight.js'),
    katex = require('katex'),
    markdown = require('markdown-it');

// Plugins
var math = require('./markdown-it-math'),
    footnote = require('./markdown-it-footnote');

function commonmark() {
    var renderer = markdown({
            html: true,
            linkify: false,
            typographer: true,
            highlight: highlight 
        })
        .use(math, { equation: equation })
        .use(footnote);
    
    function highlight(code, language) {
        var error, result;
    
        // Convert the language to lower case
        (language && (language = language.toLowerCase()));
    
        try {
            /* Attempt to highlight the code using the given language (if any)
                and otherwise detect the language automatically */
            result = (
                language
                    ? hljs.highlight(language, code)
                    : hljs.highlightAuto(code)
            );
            
            return ('<pre><code class="language-' + result.language + ' hljs">'
                + result.value + '</code></pre>');
            
        } catch (error) {
            /* Ignore errors (the code will be escaped
                and included in a plain code block) */
        }
        
        return ('<pre><code>'
            + renderer.utils.escapeHtml(code)
            + '</code></pre>');
    }

    function equation(code, block) {
        var error;

        try {
            /* Render the equation to HTML with KaTeX
                (MathML output is ommitted) */
            return katex.__renderToHTMLTree(code,
                { displayMode: block, throwOnError: true }).toMarkup();

        } catch (error) {
            // Ignore errors (the code will be escaped)
        }

        return renderer.utils.escapeHtml(code);
    }

    function transform(file, encoding, next) {
        var error;

        if (file.isNull()) {
            // Return without further processing
            return next(null, file);
        
        } else if (file.isStream()) {
            /* Indicate that streams are _not_ supported
                ("gulp-buffer" could, for example,
                be used to read streams into memory) */
            return next(new pluginError(
                'commonmark',
                'Stream content is not supported'
            ));

        } else if (file.isBuffer()) {
            try {
                file.contents = Buffer.from(
                    renderer.render(
                        file.contents.toString()
                    )
                );
    
                file.path = replaceExtension(file.path, '.html');
                return next(null, file);
                
            } catch (error) {
                return next(new pluginError('commonmark', error, {
                    fileName: file.path,
                    showStack: true
                }));
            }
        }

        return next();
    }

    return through.obj(transform);
}

module.exports = commonmark;
