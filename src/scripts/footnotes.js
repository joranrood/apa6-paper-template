'use strict';

(function() {
    function obtainFootnoteContainer(footnoteNode) {
        // Default to the document body if an enclosing section cannot be found
        var sectionNode = document.body,
            containerNode = null,
            currentNode,
            childNodes,
            childCount,
            childIndex;

        /* Attempt to find the nearest enclosing section element (the "closest"
            method is _not_ used due to the simplicity of the query) */
        for (currentNode = footnoteNode.parentNode;
                currentNode;
                currentNode = currentNode.parentNode) {
            if ((currentNode instanceof Element)
                    && (currentNode.tagName.toLowerCase() === 'section')) {
                sectionNode = currentNode;
                break;
            }
        }

        childNodes = sectionNode.childNodes;
        childCount = childNodes.length;

        /* Attempt to find a footnote container that is
            a direct descendant of the section element
            (the "querySelector" method is _not_ used because
            it cannot be used to find direct descendants) */
        for (childIndex = 0; (childIndex < childCount); childIndex++) {
            currentNode = childNodes[childIndex];

            if ((currentNode instanceof Element)
                    && currentNode.classList.contains('footnote-container')) {
                containerNode = currentNode;
                break;
            }
        }

        // Create a footnote container if it does _not_ exist
        if (!containerNode) {
            containerNode = document.createElement('footer');
            containerNode.setAttribute('class', 'footnote-container');
            sectionNode.appendChild(containerNode);
        }

        return containerNode;
    }

    function replaceFootnotes(footnoteNodes) {
        var footnoteCount = footnoteNodes.length,
            footnoteIndex,
            footnoteNode,
            footnoteIdentifier,
            linkNode,
            containerNode;

        for (footnoteIndex = 0;
                (footnoteIndex < footnoteCount);
                footnoteIndex++) {
            /* Footnotes are identified using a footnote prefix
                and sequential numbers (exisiting identifiers are discarded) */
            footnoteNode = footnoteNodes[footnoteIndex];
            footnoteIdentifier = ('footnote-' + (footnoteIndex + 1));
            footnoteNode.setAttribute('id', footnoteIdentifier);
            footnoteNode.classList.add('footnote--flow');

            // Create a link to the footnote
            linkNode = document.createElement('a');
            linkNode.setAttribute('href', ('#' + footnoteIdentifier));
            linkNode.setAttribute('class', 'footnote-link');

            // Add counters to the footnote and to the link
            footnoteNode.dataset.footnoteCounter
                = linkNode.dataset.footnoteTargetCounter
                = (footnoteIndex + 1);

            /* Replace the footnote with the link
                and add the footnote to the container */
            containerNode = obtainFootnoteContainer(footnoteNode);
            footnoteNode.parentNode.insertBefore(linkNode, footnoteNode);
            containerNode.appendChild(footnoteNode);
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        /* Find the footnote nodes using the "querySelectorAll"
            method instead of the "getElementsByClassName" method
            because the later returns a live element collection,
            which is problematic when moving elements around */
        replaceFootnotes(document.querySelectorAll('.footnote'));
    });
})();
