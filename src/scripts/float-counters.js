'use strict';

(function() {
    function populateCounters(targetNodes, counterIdentifier) {
        // The counter identifier _must_ be written in camel-case
        var counterProperty = (counterIdentifier + 'Counter'),
            targetCount = targetNodes.length,
            targetIndex;

        for (targetIndex = 0; (targetIndex < targetCount); targetIndex++) {
            // One is added to the index because counters are one-based
            targetNodes[targetIndex].dataset[counterProperty] = (targetIndex + 1);
        }
    }

    function populateTargetCounters(linkNodes, counterIdentifier) {
        // The counter identifier _must_ be written in camel-case
        var identifierExpression = /^#([^\s]+)$/,
            counterProperty = (counterIdentifier + 'Counter'),
            targetCounterProperty = (counterIdentifier + 'TargetCounter'),
            linkCount = linkNodes.length,
            linkIndex,
            linkNode,
            identifierMatch,
            targetNode;

        for (linkIndex = 0; (linkIndex < linkCount); linkIndex++) {
            linkNode = linkNodes[linkIndex];

            /* Extract the target identifier from the link node
                (the "href" attribute must begin with a hash sign
                and be followed by one or more non-whitespace characters) */
            identifierMatch = identifierExpression.exec(
                (linkNode.getAttribute('href') || '')
            );

            if (identifierMatch) {
                /* Find the target node and transfer its counter
                    to the link node (if a counter is not found,
                    the link node target counter is set to zero) */
                targetNode = document.getElementById(identifierMatch[1]);

                linkNode.dataset[targetCounterProperty]
                    = ((targetNode && targetNode.dataset[counterProperty])
                        ? targetNode.dataset[counterProperty] : 0);

            } else {
                // Set the target counter to zero when the target does not exist
                targetNode.dataset[targetCounterProperty] = 0;
            }
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        populateCounters(document.getElementsByTagName('table'), 'table');
        populateCounters(document.getElementsByTagName('figure'), 'figure');
        populateTargetCounters(document.querySelectorAll('.table-link[href]'), 'table');
        populateTargetCounters(document.querySelectorAll('.figure-link[href]'), 'figure');
    });
})();
