'use strict';

var fs = require('fs'),
    gulp = require('gulp');

// Plugins
var concat = require('gulp-concat'),
    cleancss = require('gulp-clean-css'),
    data = require('gulp-data'),
    mustache = require('gulp-mustache'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    shell = require('gulp-shell'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify');

// Custom plugins
var commonmark = require('./lib/gulp-commonmark');

gulp.task('compile:js', function() {
    return gulp.src([
            'src/scripts/footnotes.js',
            'src/scripts/float-counters.js'
        ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/scripts'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/scripts'));
});

gulp.task('compile:sass', function() {
    return gulp.src([
            'src/styles/*.scss',
            '!src/styles/_*.scss'
        ])
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(gulp.dest('dist/styles'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.init())
        .pipe(cleancss({ level: 1 }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/styles'));
});

gulp.task('compile:md', function() {
    return gulp.src('src/texts/*.md')
        .pipe(commonmark())
        .pipe(gulp.dest('dist/texts'));
});

gulp.task('compile:mustache', function() {
    return gulp.src('src/templates/index.mustache')
        .pipe(data(function(file, next) {
            fs.readFile('src/metadata.json', function(error, data) {
                if (error) { return next(error); }
                return next(null, JSON.parse(data));
            });
        }))
        .pipe(mustache(null, { extension: '.html' }))
        .pipe(gulp.dest('dist'));
});

gulp.task('render:html', function() {
    return gulp.src('dist/index.html', { read: false })
        .pipe(shell(
            'prince '
                + '--no-warn-css '
                + '--no-artificial-fonts '
                + '"<%= file.path %>"'
        ));
});
