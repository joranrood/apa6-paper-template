/* This plugin is based on the fenced code block and inline code rules
    of markdown-it 8.4.0 (https://github.com/markdown-it/markdown-it) */

'use strict';

function math(markdown, options) {
    markdown.renderer.rules.display_equation = renderEquation;
    markdown.renderer.rules.inline_equation = renderEquation;

    function renderEquation(tokens, index) {
        var token = tokens[index];

        /* Invoke an external callback to render the equation and encapsulate
            the (unmodified) equation if the callback is undefined or fails */
        return (
            ('<' + token.tag + markdown.renderer.renderAttrs(token) + '>')
            + ((options.equation && options.equation(token.content, token.block))
                || ('<script type="application/x-tex">' + token.content + '</script>'))
            + ('</' + token.tag + (token.block ? '>\n' : '>'))
        );
    }

    markdown.block.ruler.before(
        'fence',
        'display_equation',
        parseDisplayEquation,
        { alt: [ 'paragraph', 'reference', 'blockquote', 'list' ] }
    );

    markdown.inline.ruler.after(
        'escape',
        'inline_equation',
        parseInlineEquation
    );

    function parseDisplayEquation(state, fromLine, toLine, silent) {
        var offset = (state.bMarks[fromLine] + state.tShift[fromLine]),
            limit = state.eMarks[fromLine],
            foundEndMarker = false,
            currentLine,
            token;

        /* The line must _not_ be indented by more than three spaces
            (indented blocks are interpreted as code blocks),
            the line must consist of at least three characters,
            the line must begin with two dollar signs
            and the remainder of the line must consist exclusively of spaces */
        if (((state.sCount[fromLine] - state.blkIndent) > 3)
                || ((offset + 2) > limit)
                || (state.src.charCodeAt(offset) !== 0x24) /* $ */
                || (state.src.charCodeAt((offset + 1)) !== 0x24) /* $ */ 
                || (state.skipSpaces((offset + 2)) < limit)) { 
            return false;
        }

        if (silent) { return true; } // Confirm the equation block

        // Find the end of the equation block
        for (currentLine = (fromLine + 1);
                (currentLine < toLine);
                currentLine++) {
            offset = (state.bMarks[currentLine] + state.tShift[currentLine]);
            limit = state.eMarks[currentLine];

            /* Non-empty lines with negative indentation
                indicate the end of the equation block */
            if ((offset < limit)
                    && (state.sCount[currentLine] < state.blkIndent)) {
                break;

            // Check if the current line mirrors the initial line
            } else if (((state.sCount[currentLine] - state.blkIndent) <= 3)
                    && ((offset + 2) <= limit)
                    && (state.src.charCodeAt(offset) === 0x24) /* $ */
                    && (state.src.charCodeAt((offset + 1)) === 0x24) /* $ */
                    && (state.skipSpaces((offset + 2)) >= limit)) {
                foundEndMarker = true; // Indicate that an end marker was found
                break;
            }
        }

        token = state.push('display_equation', 'div', 0);
        token.attrs = [ [ 'class', 'display-equation' ] ];
        token.map = [ fromLine, currentLine ];

        token.content = state.getLines(
            fromLine,
            (currentLine + 1),
            state.sCount[fromLine],
            true
        );

        state.line = (foundEndMarker ? (currentLine + 1) : currentLine);
        return true;
    }

    function parseInlineEquation(state, silent) {
        var offset = state.pos,
            limit = state.posMax,
            index,
            token;

        /* The line should contain at least three more characters - "$x$"
            and it must contain a dollar sign at the current position */
        if (((offset + 2) > limit)
                || (state.src.charCodeAt(offset) !== 0x24)) { /* $ */
            return false;
        }

        for (index = (offset + 1); (index < limit); index++) {
            if (state.src.charCodeAt(index) === 0x24) { /* $ */
                break; // The end of the equation was found
            }
        }

        /* Inline equations must _not_ be empty and they must be followed
            by at least one other character (such as a line break) */
        if ((index === (offset + 1)) || (index >= limit)) {
            return false;
        }

        if (!silent) {
            token = state.push('inline_equation', 'span', 0);
            token.attrs = [ [ 'class', 'inline-equation' ] ];
            token.content = state.src.slice(offset, (index + 1));
        }

        state.pos = (index + 1);
        return true;
    }
}

module.exports = math;
