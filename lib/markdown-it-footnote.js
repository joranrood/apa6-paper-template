/* This plugin is based on markdown-it-footnote 3.0.1
    (https://github.com/markdown-it/markdown-it-footnote) */

'use strict';

function footnote(markdown) {
    markdown.block.ruler.before(
        'reference',
        'footnote_definition',
        parseFootnoteDefinition,
        { alt: [ 'paragraph', 'reference' ] }
    );
    
    markdown.inline.ruler.after(
        'image',
        'inline_footnote',
        parseInlineFootnote
    );
    
    markdown.inline.ruler.after(
        'inline_footnote',
        'footnote_reference',
        parseFootnoteReference
    );
    
    function parseFootnoteDefinition(state, fromLine, toLine, silent) {
        var offset = (state.bMarks[fromLine] + state.tShift[fromLine]),
            limit = state.eMarks[fromLine],
            tokens = [],
            index,
            character,
            label;
        
        /* The line should consist of at least five characters - "[^x]:"
            and it must begin with a left bracket, followed by a caret */
        if (((offset + 4) > limit)
                || (state.src.charCodeAt(offset) !== 0x5b) /* [ */
                || (state.src.charCodeAt((offset + 1)) !== 0x5e)) { /* ^ */
            return false;
        }
        
        /* Scan for the end of the label
            (labels must _not_ contain nested brackets) */
        for (index = (offset + 2); (index < limit); index++) {
            character = state.src.charCodeAt(index);
            
            if (character === 0x5c) { /* \ */
                index++; // Skip characters following backward slashes
            } else if (character === 0x5b) { /* [ */
                return false; // Nested brackets are _not_ permitted
            } else if (character === 0x5d) { /* ] */
                break;
            }
        }
        
        /* Footnote labels must _not_ be empty and they must be followed
            by a colon and at least one other character */
        if ((index === (offset + 2))
                || (++index >= limit)
                || (state.src.charCodeAt(index) !== 0x3a)) { /* : */
            return false;
        }
        
        // Extract the footnote label and normalise it
        label = markdown.utils.normalizeReference(
            state.src.slice(
                (offset + 2),
                (index - 1)
            )
        );
        
        if (!label) { return false; } // Return if the trimmed label is empty
        if (silent) { return true; } // Confirm the footnote definition
        
        // Advance past any whitespace following the colon
        while (++index < limit) {
            character = state.src.charCodeAt(index);
        
            if (!markdown.utils.isSpace(character)) {
                break;
            }
        }
        
        // Extract inline tokens from the remainder of the line
        state.md.inline.parse(
            state.src.slice(index, limit),
            state.md,
            state.env,
            tokens
        );
          
        /* Initialise footnote definitions (if necessary)
            and store the label along with the inline tokens */
        (state.env.footnotes || (state.env.footnotes = {}));
        state.env.footnotes[label] = { label: label, tokens: tokens };
        
        // Advance the parser to the next line
        state.line = (fromLine + 1);
        return true;
    }

    function parseInlineFootnote(state, silent) {
        var offset = state.pos,
            limit = state.posMax,
            tokens = [],
            index,
            token;
        
        /* The line should have at least three more characters - "^[x"
            and it must contain a caret followed by a left bracket
            at the current position */
        if (((offset + 2) > limit)
                || (state.src.charCodeAt(offset) !== 0x5e) /* ^ */
                || (state.src.charCodeAt((offset + 1)) !== 0x5b)) { /* [ */
            return false;
        }
        
        /* Check that the left and right brackets in the footnote balance out
            and and find the index of the last right bracket */
        index = markdown.helpers.parseLinkLabel(state, (offset + 1));
        if (index < 0) { return false; }
        
        if (!silent) {
            state.pos = (offset + 2);
            state.posMax = index;
            
            /* Insert `footnote_open` and `footnote_close` tags
                around the tokenised footnote contents */
            token = state.push('footnote_open', 'span', 1);
            token.attrs = [ [ 'class', 'footnote' ] ];
            state.md.inline.tokenize(state);
            state.push('footnote_close', 'span', -1);
        }
        
        state.pos = (index + 1);
        state.posMax = limit;
        return true;
    }
    
    function parseFootnoteReference(state, silent) {
        var offset = state.pos,
            limit = state.posMax,
            index,
            character,
            label,
            definition,
            token,
            tokens,
            tokenCount,
            tokenIndex;
    
        /* The line should contain at least four more characters - "[^x]"
            and it must contain left bracket followed by a caret
            at the current position */
        if (((offset + 3) > limit)
                || (state.src.charCodeAt(offset) !== 0x5b) /* [ */
                || (state.src.charCodeAt((offset + 1)) !== 0x5e)) { /* ^ */
            return false;
        }
        
        /* Scan for the end of the label
            (labels must _not_ contain line breaks
            or nested brackets) */
        for (index = (offset + 2); (index < limit); index++) {
            character = state.src.charCodeAt(index);
            
            if ((character === 0x0a) /* LINE BREAK */
                    || (character === 0x5b)) { /* [ */
                return false;
                
            } else if (character === 0x5c) { /* \ */
                index++; // Skip characters following backward slashes
            } else if (character === 0x5d) { /* ] */
                break;
            }
        }
        
        /* Footnote labels must _not_ be empty and they must be followed
            by at least one other character (such as a line break) */
        if ((index === (offset + 2)) || (index >= limit)) {
            return false;
        }
        
        /* Extract the footnote label, normalise it
            and construct a token with the footnote label */
        label = markdown.utils.normalizeReference(
            state.src.slice((offset + 2), index)
        );
        
        if (!label) { return false; }
        
        if (!silent) {
            /* Initialise footnote definitions (if necessary)
                and obtain the footnote definition */
            (state.env.footnotes || (state.env.footnotes = {}));
            definition = (state.env.footnotes[label] || null);
            
            token = state.push('footnote_open', 'span', 1);
            token.attrs = [ [ 'class', 'footnote' ] ];
            
            if (definition && definition.tokens) {
                tokens = definition.tokens;
                tokenCount = tokens.length; // Store the number of tokens
            
                /* Insert the definition tokens (if any) in-between
                    the footnote tags. Footnotes without matching definitions
                    are rendered as empty tags */
                for (tokenIndex = 0; (tokenIndex < tokenCount); tokenIndex++) {
                    state.tokens.push(tokens[tokenIndex]);
                }

                // Only allow footnote tokens to be copied once
                tokens.length = 0;
            }
            
            state.push('footnote_close', 'span', -1);
        }
        
        state.pos = (index + 1);
        state.posMax = limit;
        return true;
    }
}

module.exports = footnote;
